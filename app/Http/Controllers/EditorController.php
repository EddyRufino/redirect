<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EditorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('editor'); //para que el otro no pueda entrar asi como el editor
    }

    public function index()
    {
        return view('admin.editor');
    }
}
